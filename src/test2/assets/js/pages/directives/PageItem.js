angular.module('myApp.directives', [])
  .directive('pageItem', function($compile) {
    var sectionOneTemplate = '<input type="text" ng-model="content.title" placeholder="Título" class="page-title-control page-control"><input type="text" ng-model="content.subtitle" placeholder="Sub Título" class="page-subtitle-control page-control"><div class="page-item-img-container"><input type="text" ng-model="content.img[0].src"><img src="{{content.img[0].src}}"></div>';
    var sectionTwoTemplate = '<label>Cor de fundo:</label><input type="text" ng-model="content.color" placeholder="#ffffff" class="page-control"><input type="text" ng-model="content.title" placeholder="Título" class="page-title-control page-control"><input type="text" ng-model="content.subtitle" placeholder="Sub Título" class="page-subtitle-control page-control"><div class="page-item-button-wrapper"><a class="btn" href="{{ content.button.link }}" target="content.button.target">{{ content.button.text }}</a></div><div><label for="page-button-text">Texto do botão:</label><input type="text" id="page-button-text" ng-model="content.button.text" class="page-control"><label for="page-button-link">Link do botão:</label><input type="text" ng-model="content.button.link" id="page-button-link" class="page-control"><label><input type="radio" name="btn_target" ng-model="content.button.target" value="_blank">Abrir em nova aba</label><label><input type="radio" name="btn_target" ng-model="content.button.target" value="">Abrir na mesma aba</label></div>';
    var sectionThreeTemplate = '<label>Data:</label><input type="date" ng-model="content.date" class="page-control"><div class="page-item-img-container" ng-repeat="img in content.img"><input type="text" ng-model="img.src"><img src="{{img.src}}"></div>';

    var getTemplate = function(templateType) {
      var template = '';

      switch(templateType) {
        case 'one':
          template = sectionOneTemplate;
          break;
        case 'two':
          template = sectionTwoTemplate;
          break;
        case 'three':
          template = sectionThreeTemplate;
          break;
      }

      return '<div style="background: {{content.color}}" class="page-item-content clearfix">' + template + ' <a href="#" class="page-item-remove" ng-click="remove(index)">X</a></div>';
    }

    var linker = function($scope, element, attrs) {
      element.html(getTemplate($scope.template));

      $compile(element.contents())($scope);
    }

    return {
        restrict: "E",
        link: linker,
        scope: {
            content: '=',
            template: '=',
            remove: '&onRemove'
        }
    };
  });
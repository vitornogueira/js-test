angular.module('myApp.services', [])
  .factory('TemplateService', function() {
    var items = [
      {
        template: 'one',
        name: 'Seção 1',
        data: {
          title: '',
          subtitle: '',
          img: [
            {
              src: "http://www.ignicaodigital.com.br/wp-content/themes/ignicaodigital/images/logo/logo_ignicao_digital.png"
            }
          ]
        }
      },
      {
        template: 'two',
        name: 'Seção 2',
        data: {
          color: '#ffffff',
          title: '',
          subtitle: '',
          button: {
            text: 'Texto',
            link: 'http://www.ignicaodigital.com.br/',
            target: ''
          }
        }
      },
      {
        template: 'three',
        name: 'Seção 3',
        data: {
          date: '',
          img: [
            {
              src: "http://www.ignicaodigital.com.br/wp-content/themes/ignicaodigital/images/logo/logo_ignicao_digital.png"
            },
            {
              src: "http://www.ignicaodigital.com.br/wp-content/themes/ignicaodigital/images/logo/logo_ignicao_digital.png"
            },
            {
              src: "http://www.ignicaodigital.com.br/wp-content/themes/ignicaodigital/images/logo/logo_ignicao_digital.png"
            }
          ]
        }
      }
    ];

    return {
      getItems: function() {
        return items;
      }
    }
  });
angular.module('myApp.controllers', ['ang-drag-drop', 'hljs'])
  .controller('MainCtrl', function($parse, TemplateService) {
    var self = this;

    self.items = TemplateService.getItems();
    self.pageItems = [];

    self.jsonToString = function(source) {
      var json = $parse(angular.toJson(source))({});
      return JSON.stringify(json, null, 2);
    }

    self.removeItem = function (index) {
      self.pageItems.splice(index, 1);

      angular.forEach(self.pageItems, function(value, key) {
        self.pageItems[key].id = key;
      });
    };

    self.onDrop = function(target, source){
      self.addItem(source);
    };

    self.addItem = function(item) {
      self.pageItems.push({
        id: self.pageItems.length,
        template: item.template,
        data: item.data
      });
    };
  });
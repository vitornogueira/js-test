module.exports = function (grunt) {

  'use strict';

  grunt.initConfig({

    sass: {

      dist: {
        options: {
          style: 'compressed'
        },
        files: [{
          expand: true,
          cwd: '_assets/sass/',
          src: ['*.scss'],
          dest: 'assets/css/',
          ext: '.css'
        }]
      }

    }, // compass

    watch: {
      options: {
        spawn: false,
        livereload: true
      },
      css: {
        files: ['_assets/sass/**/*.scss'],
        tasks: ['sass']
      }
    } // watch

  });

  // Load all plugins
  require('load-grunt-tasks')(grunt);

  //Tasks
  grunt.registerTask('default', ['sass']);

  //Watch task
  grunt.registerTask('w', ['watch']);


};
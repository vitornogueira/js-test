## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?
  - Padrões JavaScript (Stoyan Stefanov) e WEB DESIGN RESPONSIVO (Mauricio Samy Silva)

2. Quais foram os últimos dois framework javascript que você trabalhou?
  - AngularJS e EmberJS

3. Descreva os principais pontos positivos de seu framework favorito.
  - Meu framework JavaScript favorito é o AngularJS. Para mim, alguns de seus pontos positivos são: Two-way data binding, injeção de dependência, arquitetura modular, templates em HTML puro.

4. Descreva os principais pontos negativos do seu framework favorito.
  - Algumas pessoas me disseram que não gostam da sintaxe do AngularJS por escrever a lógica no template (ng-repeat, ng-if, ng-switch), mas eu não vejo problemas nisso.

5. O que é código de qualidade para você?
  - É um código organizado, padronizado e conciso.


## Conhecimento de desenvolvimento

1. O que é GIT?
  - Sistema de Controle de Versão Distríbuido criado por Linus Torvalds e a comunidade de desenvolvedores do Linux em 2005.

2. Descreva um workflow simples de trabalho utilizando GIT.
  - Centralized Workflow (Fluxo de trabalho centralizado): O desenvolvedor utiliza apenas um branch (master). Ele clona o projeto para sua máquina, realiza as alterações, faz o commit, realiza o `pull` para mesclar possíveis alterações no repositório central e realiza o `push` para enviar as alterações para o repositório central.
  > Referências: http://imasters.com.br/desenvolvimento/quatro-workflows-para-trabalhar-com-git-melhores-2013/

3. Como você avalia seu grau de conhecimento em Orientação a objeto?
  - Intermediário

4. O que é Dependency Injection?
  - É um conceito utilizado basicamente para promover a reutilização, a modularidade e testabilidade do código. Ela define que, em vez de criar uma instância de um serviço dependente quando esse for necessário, a clase ou função deverá solicitá-lo.
  > Referências: Desenvolvendo com AngularJS (Shyam Seshadri & Brad Green)

5. O significa a sigla S.O.L.I.D?
  - São 5 príncipios de programação orientada a objetos e design.
    - S: Single Responsability Principle
    - 0: Open/Close Principle
    - L: Liskov Substitution Principle
    - I: Interface Segregation Principle
    - D: Dependency Inversion Principle

6. O que é BDD e qual sua importância para o processo de desenvolvimento?
  - Behavior Driven Development (Desenvolvimento Orientado a comportamento), seu foco é obter um código testado a partir de um conjunto de cenários que descrevem como a aplicação ou unidade de código deverá se comportar em determinada situação.
  - Qualquer parte interessada no projeto (stakeholders) podem escrever os testes de comportamento.
  - O BDD é importante porque produz um feedback rápido, permitindo assim saber se os requisitos descritos através dos comportamentos estão funcionais com mas agilidade.
  > Referências: http://tableless.com.br/introducao-ao-behavior-driven-development/, http://eduardopires.net.br/2012/06/ddd-tdd-bdd/

7. Fale sobre Design Patterns.
  - São padrões de projeto, focam na reutilização de soluções.
  - Padrões para soluções comuns.
  - São melhores práticas no desenvolvimento de software que nascem das experiências e conhecimento dos desenvolvedores.
  - Príncipios comuns: KISS (Keep It Simple Stupid), DRY (Don't Repeat Yourself), YAGNI (You Ain't Gonna Need It)
  > Referências: http://www.princiweb.com.br/blog/programacao/design-patterns/o-que-sao-design-patterns.html, http://pt.wikipedia.org/wiki/Padr%C3%A3o_de_projeto_de_software

9. Discorra sobre práticas que você considera boas no desenvolvimento javascript.
  - Utilização de um *style guide* para manter um padrão de código entre os membros da equipe.
  - Modularizar o código.
  - Ser consistente no padrão e estilo do código.
  - Acessar ao DOM somente quando nevessário.
  - Evitar globais, utilizando closures ou algum pattern.
  - Usar nome de variáveis e funções autoexplicativas.